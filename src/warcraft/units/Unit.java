package warcraft.units;

import warcraft.units.interfaces.Destroyable;
import java.util.Random;
import warcraft.Map;

/**
 *
 * @author sam
 */
public abstract class Unit implements Destroyable{
    /*
    *
    */
    private String name = "Louis";
    private int hp = 42;
    private int mana = 0;

    
    public Unit(){
        this.name = "Louis";
        this.hp = 42;
        this.mana = 0;
    }
    
    public Unit(String name, int hp, int mana) {
        this.name = name;
        this.hp = hp;
        this.mana = mana;
    }
    
    public static int randInt(int min, int max){
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    @Override
    public String toString() {
        return String.format("Hi, i'm a %s, my name is %s!", this.getClass(), this.getName());
    }

    
    public abstract String talk();
    
    public void dance(){
        System.out.println("Let's dance !");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    @Override
    public void takeDamage(int damage) {
        this.hp = this.getHp() - damage;
        String message = String.format("Unit: %s take damage: %s", this, damage);
        System.out.println(message);
        
        if(this.getHp() <= 0){
            Map.getInstance().removeUnit(this);
        }
    }
}
