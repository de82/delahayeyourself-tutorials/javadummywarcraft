package warcraft.units.orcs;

import warcraft.units.interfaces.IsStupid;
import warcraft.units.interfaces.IsWarrior;
import warcraft.units.Unit;

/**
 *
 * @author sam
 */
public class Grunt extends Unit implements IsWarrior, IsStupid{

    public Grunt(){
        super();
    }
    
    public Grunt(String name, int hp, int mana){
        super(name, hp, mana);
    }
    
    @Override
    public String talk() {
        return "Hello I'm a grunt !";
    }

    @Override
    public void attack(Unit unit) {
        System.out.println(String.format("Grunt: %s attak unit: %s", this, unit));
        unit.takeDamage(12);
    }

    @Override
    public void walk() {
        System.out.println("I'm Walking !");
    }

    @Override
    public void doSomethindStupid() {
        System.out.println("I'm stupid !");
    }

}
