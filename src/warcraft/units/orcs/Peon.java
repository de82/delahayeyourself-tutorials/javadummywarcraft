package warcraft.units.orcs;

import warcraft.units.Unit;

/**
 *
 * @author sam
 */
public class Peon extends Unit{
    
    public Peon(){
        super();
    }
    
    public Peon(String name, int hp, int mana){
        super(name, hp, mana);
    }

    @Override
    public String talk() {
        return "Hello I'm a Peon !";
    }

}
