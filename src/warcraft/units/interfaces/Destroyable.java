package warcraft.units.interfaces;

/**
 *
 * @author sam
 */
public interface Destroyable {
    public void takeDamage(int damage);
}
