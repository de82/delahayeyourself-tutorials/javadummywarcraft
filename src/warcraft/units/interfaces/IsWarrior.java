package warcraft.units.interfaces;

import warcraft.units.Unit;

/**
 *
 * @author sam
 */
public interface IsWarrior {
    public void attack(Unit unit);
    public void walk();
}
