package warcraft;

import warcraft.units.orcs.Grunt;
import warcraft.units.orcs.Peon;

/**
 *
 * @author sam
 */
public class Warcraft {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Peon peon = new Peon();
        Grunt grunt = new Grunt();
        
        System.out.println(peon);
        System.out.println(grunt);
        
        System.out.println(peon.talk());
        System.out.println(grunt.talk());
        
        grunt.attack(peon);
        
        Map.getInstance().addUnit(grunt);
        Map.getInstance().addUnit(peon);
    }
    
}
