package warcraft;

import java.util.ArrayList;
import warcraft.units.Unit;

/**
 *
 * @author sam
 */
public class Map {
    
    private static Map instance = null;
    private ArrayList<Unit> units;
    
    private Map(){
        this.units = new ArrayList<Unit>();
    }
    
    public static Map getInstance(){
        if(null == instance){
            instance = new Map();
        }
        return instance;
    }
    
    public void addUnit(Unit unit){
        this.units.add(unit);
    }
    
    
    public void removeUnit(Unit unit){
        this.units.remove(unit);
    }
}
