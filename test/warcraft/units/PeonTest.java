package warcraft.units;

import org.junit.Test;
import static org.junit.Assert.*;
import warcraft.units.orcs.Peon;
import warcraft.units.interfaces.Destroyable;

/**
 *
 * @author sam
 */
public class PeonTest {
    
    @Test
    public void testPeonConstructor(){
        Peon peon = new Peon();
        Peon peon2 = new Peon("George", 22, 12);
        
        assertEquals("Louis", peon.getName());
        assertEquals(42, peon.getHp());
        assertEquals(0, peon.getMana());
        
        assertEquals("George", peon2.getName());
        assertEquals(22, peon2.getHp());
        assertEquals(12, peon2.getMana());
    }
    
    @Test
    public void testPeonToString(){
        Peon peon = new Peon();
        Peon peon2 = new Peon("George", 22, 12);
        
        assertEquals("Hi, i'm a class warcraft.units.orcs.Peon, my name is Louis!", peon.toString());
        assertEquals("Hi, i'm a class warcraft.units.orcs.Peon, my name is George!", peon2.toString());
    }
    
    @Test
    public void testPeonTakeDamage(){
        Peon peon = new Peon("George", 20, 0);
        
        assertEquals(true, peon instanceof Unit);
        assertEquals(true, peon instanceof Destroyable);
        assertEquals(20, peon.getHp());
        peon.takeDamage(2);
        assertEquals(18, peon.getHp());
    }
}
