package warcraft.units;

import org.junit.Test;
import static org.junit.Assert.*;
import warcraft.units.interfaces.Destroyable;
import warcraft.units.interfaces.IsWarrior;
import warcraft.units.orcs.Grunt;

/**
 *
 * @author sam
 */
public class GruntTest {
    
    @Test
    public void testGruntAttack(){
        Grunt grunt = new Grunt("Louis", 20, 0);
        
        assertEquals(true, grunt instanceof Unit);
        assertEquals(true, grunt instanceof Destroyable);
        assertEquals(true, grunt instanceof IsWarrior);
        
        grunt.attack(grunt);
        assertEquals(8, grunt.getHp());
    }
}
